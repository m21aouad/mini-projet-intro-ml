import pandas as pd
import unittest
from binary_classification_workflow import *

class UnitTests(unittest.TestCase):
    
    def set_up_df(self):
        # Load the first 100 samples of the kidney dataset for testing
        self.df = pd.read_csv('./data/kidney_disease.csv').head(100)
    
    # Test if the resulting categorical columns are as expected
    def test_get_categorical_columns(self):
        categorical_columns = get_categorical_columns(self.df)
        expected_columns = ['rbc', 'pc', 'pcc', 'ba', 'pcv', 'wc', 'rc', 'htn', 'dm', 'cad', 'appet', 'pe', 'ane', 'classification']
        self.assertEqual(categorical_columns, expected_columns)
    
    # Test if the resulting numerical columns are as expected
    def test_get_numerical_columns(self):
        numerical_columns = get_numerical_columns(self.df)
        expected_columns = ['age', 'bp', 'sg', 'al', 'su', 'bgr', 'bu', 'sc', 'sod', 'pot', 'hemo']
        self.assertFalse('id' in self.df.columns)  # Ensure 'id' column is not present
        self.assertEqual(numerical_columns, expected_columns)
    
    # Test if filling missing values with median works as expected
    def test_fill_median(self):
        fill_median(self.df, 'bgr')
        self.assertFalse(self.df['bgr'].isnull().any()) 
    
    def test_fill_mean(self):
        fill_mean(self.df, 'sod')
        self.assertFalse(self.df['sod'].isnull().any()) 
    
    def test_is_skewed(self):
        skewed = is_skewed(self.df, 'hemo')
        self.assertFalse(skewed)  # 'hemo' column is not skewed
    
    # Test if the number of columns in the resulting dataframe after encoding the categorical features is greater than the initial 
    # number of columns
    def test_encode_categorical(self):
        categorical_cols = get_categorical_columns(self.df)
        result_df = convert_categorical_feats(self.df, categorical_cols)
        self.assertTrue(len(result_df.columns) > len(self.df.columns))

if __name__ == '__main__':
    unittest.main()
